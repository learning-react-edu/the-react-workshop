import React, { Component } from 'react';
import './App.css';

const Greeting = props => <p>Hello, {props.name}!</p>

/*
const App = () => (
    <div>
        <h1>My App</h1>
        <br/>
        <Greeting name="React" />
    </div>
);
*/

class App extends Component {
    constructor(props) {
        super(props);
        this.title = 'React App';
        this.state = { clickCounter: 0 };
    }

    renderClickCount() {
        return <p>I've been clicked {this.state.clickCounter} times!</p>;
    }

    render() {
        return (
            <div>
                <h1>{this.title}</h1>
                <br/>
                <Greeting name="React" />
                <br />
                {this.renderClickCount()}
            </div>
        );
    }
}

export default App;
