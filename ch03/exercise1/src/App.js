import './App.css';
import {Component} from "react";

class App extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            showSecret: false
        }
    }

    secretMessage() {
        if (!this.state.showSecret) {
            return;
        }
        return (
            <div className="secret-message">
                I am the secret message!
            </div>
        )
    }

    toggleSecretMessage() {
        this.setState({
            showSecret: !this.state.showSecret
        });
    }

    render() {
        return (
            <div className="App">
                <button onClick={this.toggleSecretMessage.bind(this)}>
                    Click to { this.state.showSecret ? "hide" : "show" } the secret message!
                </button>
                {this.secretMessage()}
            </div>
        );
    }
}

export default App;
