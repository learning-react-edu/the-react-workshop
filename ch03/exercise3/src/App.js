import './App.css';
import {Component} from "react";

const TILE_COUNT = 12;

class App extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            tiles: [],
            lastFlipped: null,
            clicksCount: 0,
        };
        this.resetTiles = this.resetTiles.bind(this);
        this.flipTile = this.flipTile.bind(this);
    }

    flipAllBackOver(tiles) {
        tiles.forEach(tile => {
            if (!tile.matched) {
                tile.flipped = true;
            }
        });
        return tiles;
    }

    flipTile(index) {
        let tiles = this.state.tiles;
        let tile = tiles[index];
        let clicksCount = this.state.clicksCount + 1;
        let lastFlipped = this.state.lastFlipped;

        if (lastFlipped == null) {
            tiles = this.flipAllBackOver(tiles);
            tile.flipped = !tile.flipped;
            lastFlipped = index;
        } else {
            tile.flipped = !tile.flipped;
            let lastFlippedTile = this.state.tiles[lastFlipped];
            if (lastFlippedTile.number === tile.number) {
                lastFlippedTile.matched = true;
                tile.matched = true;
                tiles[lastFlipped] = lastFlippedTile;
            }
            lastFlipped = null;
        }
        tiles[index] = tile;
        this.setState({clicksCount, tiles, lastFlipped});
    }

    resetTiles() {
        let tiles = [];
        let number = 0;

        for (let i = 0; i < TILE_COUNT; i += 2) {
            number++;
            let tileOne = {flipped: true, matched: false, number};
            let tileTwo = {flipped: true, matched: false, number};
            tiles = [...tiles, tileOne, tileTwo];
        }

        for (let i = 0; i < tiles.length; i++) {
            const swapWith = Math.floor(Math.random() * tiles.length);
            [tiles[i], tiles[swapWith]] = [tiles[swapWith], tiles[i]];
        }

        this.setState({clicksCount: 0, tiles});
    }

    renderTile(tile, index) {
        let classes = ["Tile"];

        if (tile.flipped) {
            classes = [...classes, "flipped"];
        }
        if (tile.matched) {
            classes = [...classes, "matched"];
        }

        let key = `tile-${index}`;

        return (
            <div key={key} className={classes.join(" ")} onClick={() => this.flipTile(index)}>
                {!tile.flipped && tile.number}
            </div>
        );
    }

    render() {
        return (
            <div>
                <h1>Memory Game</h1>
                <strong>Clicks: {this.state.clicksCount}</strong>
                <br/>
                <button onClick={this.resetTiles} className="reset">New Game</button>
                <hr/>
                {this.state.tiles.map((tile, index) => this.renderTile(tile, index))}
            </div>
        );
    }
}

export default App;
