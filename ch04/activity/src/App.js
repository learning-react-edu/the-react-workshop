import './App.css';
import UserCountDisplay from './UserCountDisplay';
import LoginDisplay from './LoginDisplay';

function App() {
  return (
    <div className="App">
        <h1>Messaging App</h1>
        <UserCountDisplay />
        <LoginDisplay />
    </div>
  );
}

export default App;
